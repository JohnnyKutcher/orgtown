import org.hibernate.Session;
import org.town.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("all")
public class HibernateStandAlone {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
//
//        Apartment apartment = new Apartment(3, 10,true);
//        Room room = new Room(2, 36);
//
//        session.beginTransaction();
//
//        session.persist(apartment);
//
//        apartment.setRoom(room);
//        session.save(apartment);
//
//        List<Apartment> apartments = (List<Apartment>) session.createQuery("from Apartment ").list();
//        for (Apartment a: apartments) {
//            System.out.println("Detail: " + a);
//        }
//
//        session.getTransaction().commit();
//
//        session.close();


//        Apartment apartment1 = new Apartment(3,10, true);
//        Apartment apartment2 = new Apartment(8,83, false);
//
//        House house = new House(9, 4);
//        Room room = new Room(3, 36);
//
//        List<Apartment> list = new ArrayList<>();
//
//        apartment1.setHouse(house);
//        apartment1.setRoom(room);
//
//        apartment2.setHouse(house);
//        apartment2.setRoom(room);
//
//        list.add(apartment1);
//        list.add(apartment2);
//
//        house.setApartments(list);
//
//        session.beginTransaction();
//
//        session.persist(room);
//
//        session.persist(house);
//
//        List<Apartment> apartments = (List<Apartment>)session.createQuery("from Apartment ").list();
//
//        for (Apartment apartment:apartments) {
//            System.out.println("Apartament details: " + apartment);
//            System.out.println("House details: " + apartment.getHouse() + " and Room details: " + apartment.getRoom());
//        }
//
//        session.getTransaction().commit();
//        session.close();


//        Street street = new Street("Shevchenko");
//
//        House house1 = new House(9,4);
//        House house2 = new House(4,3);
//
//        Apartment apartment1 = new Apartment(3, 10, true);
//        Apartment apartment2 = new Apartment(1, 8, false);
//        Apartment apartment3 = new Apartment(12, 123, true);
//        Apartment apartment4 = new Apartment(14, 140, false);
//
//        Room room1 = new Room(3, 36);
//
//        List<Apartment> apartments1 = new ArrayList<>();
//
//        apartment1.setRoom(room1);
//        apartment1.setHouse(house1);
//
//        apartment2.setRoom(room1);
//        apartment2.setHouse(house1);
//
//        apartments1.add(apartment1);
//        apartments1.add(apartment2);
//
//        List<Apartment> apartments2 = new ArrayList<>();
//
//        apartment3.setRoom(room1);
//        apartment3.setHouse(house2);
//
//        apartment4.setRoom(room1);
//        apartment4.setHouse(house2);
//
//        apartments2.add(apartment3);
//        apartments2.add(apartment4);
//
//        house1.setApartments(apartments1);
//        house2.setApartments(apartments2);
//
//        house1.setStreet(street);
//        house2.setStreet(street);
//
//        List<House> houses = new ArrayList<>();
//        houses.add(house1);
//        houses.add(house2);
//
//        street.setHouses(houses);
//
//        session.beginTransaction();
//
//        session.persist(street);
//
//        List<Street> streets = (List<Street>)session.createQuery("from Street ").list();
//
//        for (Street s: streets) {
//            System.out.println("Details of street: " + s);
//        }
//
//        session.getTransaction().commit();
//        session.close();

        Street street4 = new Street("Verhovinna");
        Street street5 = new Street("Mazepi");

        Town town1 = new Town("Kiev");
        Town town2 = new Town("Dnipro");
        Town town3 = new Town("Lviv");

        House house3 = new House(5, 2);
        House house4 = new House(25, 5);
        House house5 = new House(9, 3);

        Apartment apartment5 = new Apartment(4, 42, false);
        Apartment apartment6 = new Apartment(18, 185, true);
        Apartment apartment7 = new Apartment(1, 8, false);

        Room room2 = new Room(3, 36);

        street4.getTowns().add(town1);
        street4.getTowns().add(town2);

        street5.getTowns().add(town1);
        street5.getTowns().add(town2);
        street5.getTowns().add(town3);

        List<Apartment> apartments2 = new ArrayList<>();

        apartment5.setRoom(room2);
        apartment5.setHouse(house3);

        apartments2.add(apartment5);


        List<Apartment> apartments3 = new ArrayList<>();

        apartment6.setRoom(room2);
        apartment6.setHouse(house4);

        apartments3.add(apartment6);

        List<Apartment> apartments4 = new ArrayList<>();

        apartment7.setRoom(room2);
        apartment7.setHouse(house5);

        apartments4.add(apartment7);

        house3.setApartments(apartments2);
        house3.setStreet(street4);

        house4.setApartments(apartments3);
        house4.setStreet(street5);

        house5.setApartments(apartments4);
        house5.setStreet(street4);

        List<House> houses2 = new ArrayList<>();
        houses2.add(house3);
        houses2.add(house5);

        street4.setHouses(houses2);

        List<House> houses3 = new ArrayList<>();
        houses3.add(house4);

        street5.setHouses(houses3);

        session.beginTransaction();

        session.persist(street4);
        session.persist(street5);

        session.getTransaction().commit();
        session.close();
    }
}

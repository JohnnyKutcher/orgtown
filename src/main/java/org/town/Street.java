package org.town;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@SuppressWarnings("all")
@Table(name = "STREET")
public class Street {

    @Id
    @GeneratedValue
    @Column(name = "STREET_ID")
    private long id;

    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "street", cascade = CascadeType.ALL)
    private List<House> houses;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "STREET_OF_TOWN",
                joinColumns = {@JoinColumn(name = "STREET_ID")},
                inverseJoinColumns = {@JoinColumn(name = "TOWN_ID")})
    private List<Town> towns = new ArrayList<>();

    public Street() {
    }

    public Street(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<House> getHouses() {
        return houses;
    }

    public void setHouses(List<House> houses) {
        this.houses = houses;
    }

    public List<Town> getTowns() {
        return towns;
    }

    public void setTowns(List<Town> towns) {
        this.towns = towns;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Street))
            return false;
        Street other = (Street) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Street{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", houses=" + houses +
                '}';
    }
}

package org.town;

import javax.persistence.*;

@Entity
@SuppressWarnings("all")
@Table(name = "ROOM")
public class Room {

    @Id
    @Column(name = "ROOM_ID")
    @GeneratedValue
    private long id;

    @Column(name = "HEIGHT_OF_WALLS")
    private int heightOfWalls;

    @Column(name = "AREA")
    private int area;

    public Room() {
    }

    public Room(int heightOfWalls, int area) {
        this.heightOfWalls = heightOfWalls;
        this.area = area;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getHeightOfWalls() {
        return heightOfWalls;
    }

    public void setHeightOfWalls(int heightOfWalls) {
        this.heightOfWalls = heightOfWalls;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", heightOfWalls=" + heightOfWalls +
                ", area=" + area +
                '}';
    }
}

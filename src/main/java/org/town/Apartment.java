package org.town;

import javax.persistence.*;
import java.util.Objects;

@Entity
@SuppressWarnings("all")
@Table(name = "APARTMENT")
public class Apartment {

    @Id
    @GeneratedValue
    @Column(name = "APARTMENT_ID")
    private long id;

    @Column(name = "FLOOR")
    private int floor;

    @Column(name = "NUMBER")
    private int number;

    @Column(name = "BALCONY")
    private boolean balcony;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "APARTMENT_ROOM_ID")
    private Room room;

    @ManyToOne(optional = false)
    @JoinColumn(name = "HOUSE_ID")
    private House house;

    public Apartment() {
    }

    public Apartment(int floor, int number, boolean balcony) {
        this.floor = floor;
        this.number = number;
        this.balcony = balcony;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isBalcony() {
        return balcony;
    }

    public void setBalcony(boolean balcony) {
        this.balcony = balcony;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Apartment))
            return false;
        Apartment other = (Apartment) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Apartment{" +
                "id=" + id +
                ", floor=" + floor +
                ", number=" + number +
                ", balcony=" + balcony +
                ", room=" + room +
                '}';
    }
}

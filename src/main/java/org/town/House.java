package org.town;


import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@SuppressWarnings("all")
@Table(name = "HOUSE")
public class House {

    @Id
    @GeneratedValue
    @Column(name = "HOUSE_ID")
    private long id;

    @Column(name = "NUMBER_OF_FLOORS")
    private int numberOfFloors;

    @Column(name = "NUMBER_OF_ENTRANCES")
    private int numberOfEntrances;

    @OneToMany(mappedBy = "house", cascade = CascadeType.ALL)
    private List<Apartment> apartments;

    @ManyToOne(optional = false)
    @JoinColumn(name = "STREET_ID")
    private Street street;

    public House() {
    }

    public House(int numberOfFloors, int numberOfEntrances) {
        this.numberOfFloors = numberOfFloors;
        this.numberOfEntrances = numberOfEntrances;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    public void setNumberOfFloors(int numberOfFloors) {
        this.numberOfFloors = numberOfFloors;
    }

    public int getNumberOfEntrances() {
        return numberOfEntrances;
    }

    public void setNumberOfEntrances(int numberOfEntrances) {
        this.numberOfEntrances = numberOfEntrances;
    }

    public List<Apartment> getApartments() {
        return apartments;
    }

    public void setApartments(List<Apartment> apartments) {
        this.apartments = apartments;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof House))
            return false;
        House other = (House) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "House{" +
                "id=" + id +
                ", numberOfFloors=" + numberOfFloors +
                ", numberOfEntrances=" + numberOfEntrances +
                ", apartments=" + apartments +
                '}';
    }
}
